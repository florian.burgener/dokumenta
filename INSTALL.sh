#!/bin/bash
# Copyright 2021 Florian Burgener
#
# This file is part of Dokumenta.
#
# Dokumenta is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Dokumenta is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Dokumenta.  If not, see <https://www.gnu.org/licenses/>.
# This script allows you to install Dokumenta on an Ubuntu 20.04 server.

if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root"
    exit 1
fi

# Prepare the database.

apt-get install mysql-server -y

mysql_database_name="dokumenta"
mysql_user_username="dokumenta"
mysql_user_password=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 20 ; echo '')

mysql --user=root --execute="DROP DATABASE IF EXISTS $mysql_database_name; CREATE DATABASE $mysql_database_name CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
mysql --user=root --execute="DROP USER IF EXISTS $mysql_user_username@localhost; CREATE USER $mysql_user_username@localhost IDENTIFIED BY '$mysql_user_password';"
mysql --user=root --execute="GRANT ALL PRIVILEGES ON $mysql_database_name.* TO '$mysql_user_username'@'localhost';"

apt-get install python3-venv -y

alembic_config="sqlalchemy.url = mysql+pymysql:\/\/${mysql_user_username}:${mysql_user_password}@localhost\/${mysql_database_name}"
sed -i -E "s/sqlalchemy.url =.*/$alembic_config/g" src/database/migrations/alembic.ini

cd src/database/migrations
rm -rf venv
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
alembic upgrade head
deactivate

# Fill the database.

apt-get install pv -y

cd ../seeds
rm -rf venv
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
> .env
echo "DB_HOST=localhost" >> .env
echo "DB_DATABASE=${mysql_database_name}" >> .env
echo "DB_USERNAME=${mysql_user_username}" >> .env
echo "DB_PASSWORD=${mysql_user_password}" >> .env
python3 database_seeder.py
deactivate
cd ../../..

# Prepares the environment variables.

rm -rf /usr/local/bin/dokumenta
mkdir /usr/local/bin/dokumenta
cp -r src/dokumenta /usr/local/bin/dokumenta/dokumenta
cp src/__main__.py /usr/local/bin/dokumenta/__main__.py
> /usr/local/bin/dokumenta/.env
app_key=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 20 ; echo '')
echo "APP_KEY=${app_key}" >> /usr/local/bin/dokumenta/.env
echo "DB_HOST=localhost" >> /usr/local/bin/dokumenta/.env
echo "DB_DATABASE=${mysql_database_name}" >> /usr/local/bin/dokumenta/.env
echo "DB_USERNAME=${mysql_user_username}" >> /usr/local/bin/dokumenta/.env
echo "DB_PASSWORD=${mysql_user_password}" >> /usr/local/bin/dokumenta/.env

# Installs dependencies.

apt-get install build-essential libpoppler-cpp-dev pkg-config python3-dev -y

rm -rf /usr/local/bin/dokumenta/venv
python3 -m venv /usr/local/bin/dokumenta/venv
source /usr/local/bin/dokumenta/venv/bin/activate
pip install -r src/requirements.txt

python3 << END
import nltk
nltk.download('punkt')
nltk.download('wordnet')
END

# Creates data folders and copies the configuration.

mkdir -p /etc/dokumenta
cp dokumenta.ini /etc/dokumenta/dokumenta.ini

mkdir -p /var/lib/dokumenta
mkdir -p /var/lib/dokumenta/documents

# Creates the service.

> /etc/systemd/system/dokumenta.service
echo "[Unit]" >> /etc/systemd/system/dokumenta.service
echo "After=mysql.service" >> /etc/systemd/system/dokumenta.service
echo "" >> /etc/systemd/system/dokumenta.service
echo "[Service]" >> /etc/systemd/system/dokumenta.service
echo "Type=simple" >> /etc/systemd/system/dokumenta.service
echo "Restart=always" >> /etc/systemd/system/dokumenta.service
echo "RestartSec=5s" >> /etc/systemd/system/dokumenta.service
echo "WorkingDirectory=/usr/local/bin/dokumenta/venv" >> /etc/systemd/system/dokumenta.service
echo "ExecStart=/usr/local/bin/dokumenta/venv/bin/python /usr/local/bin/dokumenta/__main__.py --config=/etc/dokumenta/dokumenta.ini" >> /etc/systemd/system/dokumenta.service
echo "" >> /etc/systemd/system/dokumenta.service
echo "[Install]" >> /etc/systemd/system/dokumenta.service
echo "WantedBy=multi-user.target" >> /etc/systemd/system/dokumenta.service

systemctl daemon-reload
systemctl start dokumenta.service

GREEN='\033[0;32m'
echo -e "${GREEN}Dokumenta is installed and ready on http://localhost:5000"
NOCOLOR='\033[0m'
echo -e "${NOCOLOR}"
