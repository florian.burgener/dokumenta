# Dokumenta

## About Dokumenta
Dokumenta is a document categorization engine.

## Prerequisite
You must have an empty Ubuntu 20.04 server. You must also have updated it.

```shell
apt-get update
apt-get upgrade -y
apt-get install unzip -y
```

## Installation
Copy the file "Dokumenta_V1.0.1.zip" to your Ubuntu 20.04 server.

Unzipped it

```shell
unzip Dokumenta_V1.0.1.zip -d dokumenta
```

Then run the script named "INSTALL.sh".

```shell
cd dokumenta
bash INSTALL.sh
```

Dokumenta will now install itself.
