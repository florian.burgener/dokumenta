# Copyright 2021 Florian Burgener
#
# This file is part of Dokumenta.
#
# Dokumenta is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Dokumenta is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Dokumenta.  If not, see <https://www.gnu.org/licenses/>.
"""Dokumenta is a document categorization engine.

This module is the entry point of Dokumenta.
"""

import argparse

from dokumenta.server import main


if __name__ == "__main__":
    # Definition of the optional arguments of the application.
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="Path to the configuration file.")
    args = parser.parse_args()
    main(args)
