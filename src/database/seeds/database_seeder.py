#!/bin/bash
# Copyright 2021 Florian Burgener
#
# This file is part of Dokumenta.
#
# Dokumenta is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Dokumenta is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Dokumenta.  If not, see <https://www.gnu.org/licenses/>.
"""This module allows to fill the database."""

from typing import Any, Tuple
import bcrypt
import os
import pathlib
import subprocess

from dotenv import load_dotenv
import mysql.connector


class DokumentaDB:
    """Represents a connection to the Dokumenta database."""

    def __init__(self) -> None:
        """Inits DokumentaDB."""
        self.cnx = mysql.connector.connect(
            host=os.getenv("DB_HOST"),
            database=os.getenv("DB_DATABASE"),
            user=os.getenv("DB_USERNAME"),
            passwd=os.getenv("DB_PASSWORD"),
        )
        self.cursor = self.cnx.cursor()

        # Initializes various session variables.
        self.execute("SET AUTOCOMMIT = 1")
        self.execute("SET NAMES utf8mb4")
        self.execute("SET CHARACTER SET utf8mb4")
        self.execute("SET character_set_connection=utf8mb4")
        self.execute('SET time_zone="+00:00"')

    def execute(self, query: str, params: Tuple[Any, ...] = ()) -> None:
        """Executes a query on the database.

        Args:
            query (str): The request to be executed.
            params (Tuple[Any, ...]): The parameters to send with the request.
            Defaults to ().
        """
        self.cursor.execute(query, params)

    def __del__(self) -> None:
        """Closes the connection when the object is destroyed."""
        self.cnx.close()


def run_documents_seeder() -> None:
    """Runs the document seeder."""

    if not os.path.isfile("./documents.sql"):
        print("Documents seeding skipped")
        return
    absolute_path = str(pathlib.Path().absolute()) + "/" + "documents.sql"
    command = "pv " + absolute_path + " | mysql -u " + os.getenv("DB_USERNAME") + " -h " + os.getenv("DB_HOST") + " -p\"" + os.getenv("DB_PASSWORD") + "\" " + os.getenv("DB_DATABASE")
    subprocess.run(command, shell=True, check=True)


def run_roles_seeder(database: DokumentaDB) -> None:
    """Runs the role seeder.

    Args:
        database (DokumentaDB): Connection to the Dokumenta database.
    """

    query = "INSERT INTO roles (name) VALUES (%s)"

    roles = [
        ("user",),
        ("admin",),
    ]

    for role in roles:
        database.execute(query, role)


def hash_password(password: str) -> str:
    """Hash the password.

    Args:
        password (str): Password that must be hashed.

    Returns:
        str: Password hashed.
    """

    salt = bcrypt.gensalt()
    encoded_password = password.encode("utf-8")
    return bcrypt.hashpw(encoded_password, salt)


def run_users_seeder(database: DokumentaDB) -> None:
    """Runs the user seeder.

    Args:
        database (DokumentaDB): Connection to the Dokumenta database.
    """

    query = (
        "INSERT INTO users (username, password, role_id) "
        "VALUES (%s, %s, %s)"
    )

    users = [
        ("root", hash_password("rootroot"), 2),
        ("guest", hash_password("rootroot"), 1),
    ]

    for user in users:
        database.execute(query, user)


def main() -> None:
    """Entry point of the module."""

    load_dotenv()
    database = DokumentaDB()

    print("Seeder Start : Documents")
    run_documents_seeder()
    print("Seeder Stop : Documents")
    print("Seeder Start : Roles")
    run_roles_seeder(database)
    print("Seeder Stop : Roles")
    print("Seeder Start : Users")
    run_users_seeder(database)
    print("Seeder Stop : Users")


if __name__ == "__main__":
    main()
