#!/bin/bash
# Copyright 2021 Florian Burgener
#
# This file is part of Dokumenta.
#
# Dokumenta is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Dokumenta is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Dokumenta.  If not, see <https://www.gnu.org/licenses/>.
"""Create roles table.

Revision ID: 0546fe4f8c1e
Revises: 2d00af9617d2
Create Date: 2021-03-30 10:29:03.004291
"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "0546fe4f8c1e"
down_revision = "0f2466e83fb8"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "roles",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.String(100), nullable=False),
        mysql_engine="InnoDB",
    )

    op.create_unique_constraint(
        None,
        "roles",
        ["name"],
    )


def downgrade():
    op.drop_table("roles")
