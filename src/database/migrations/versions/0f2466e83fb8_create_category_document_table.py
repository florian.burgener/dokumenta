#!/bin/bash
# Copyright 2021 Florian Burgener
#
# This file is part of Dokumenta.
#
# Dokumenta is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Dokumenta is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Dokumenta.  If not, see <https://www.gnu.org/licenses/>.
"""Create category_document table.

Revision ID: 0f2466e83fb8
Revises: 6dd7341a2052
Create Date: 2021-03-30 10:28:47.982091
"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "0f2466e83fb8"
down_revision = "f29f827f04d0"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "category_document",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("category_id", sa.Integer, nullable=False),
        sa.Column("document_id", sa.Integer, nullable=False),
        mysql_engine="InnoDB",
    )

    op.create_foreign_key(
        None,
        "category_document",
        "categories",
        ["category_id"],
        ["id"],
    )

    op.create_foreign_key(
        None,
        "category_document",
        "documents",
        ["document_id"],
        ["id"],
    )


def downgrade():
    op.drop_table("category_document")
