#!/bin/bash
# Copyright 2021 Florian Burgener
#
# This file is part of Dokumenta.
#
# Dokumenta is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Dokumenta is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Dokumenta.  If not, see <https://www.gnu.org/licenses/>.
"""Create documents table.

Revision ID: f29f827f04d0
Revises:
Create Date: 2021-03-30 10:07:21.332610
"""
from alembic import op
from multiprocessing import Process
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "f29f827f04d0"
down_revision = "6dd7341a2052"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "documents",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("title", sa.Text(65535), nullable=False),
        sa.Column("relative_path", sa.String(260), nullable=False),
        sa.Column("filename", sa.String(100), nullable=False),
        sa.Column("file_type", sa.String(20), nullable=False),
        sa.Column("raw_text", sa.Text(16777215), nullable=False),
        sa.Column("clean_text", sa.Text(16777215), nullable=True),
        mysql_engine="InnoDB",
    )

    op.create_unique_constraint(
        None,
        "documents",
        ["filename"],
    )

    # op.create_table('processed_documents',
    #     sa.Column('id', sa.Integer, primary_key=True),
    #     sa.Column('uid', sa.String(100), nullable=False),
    # )
    # op.create_unique_constraint(None, 'processed_documents', ['uid'])


def downgrade():
    op.drop_table("documents")
    # op.drop_table('processed_documents')
