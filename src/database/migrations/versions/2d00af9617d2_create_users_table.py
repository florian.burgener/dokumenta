#!/bin/bash
# Copyright 2021 Florian Burgener
#
# This file is part of Dokumenta.
#
# Dokumenta is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Dokumenta is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Dokumenta.  If not, see <https://www.gnu.org/licenses/>.
"""Create users table.

Revision ID: 2d00af9617d2
Revises: 0f2466e83fb8
Create Date: 2021-03-30 10:28:58.497449
"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "2d00af9617d2"
down_revision = "0546fe4f8c1e"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "users",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("username", sa.String(100), nullable=False),
        sa.Column("password", sa.String(100), nullable=False),
        sa.Column("role_id", sa.Integer, nullable=False),
        mysql_engine="InnoDB",
    )

    op.create_foreign_key(
        None,
        "users",
        "roles",
        ["role_id"],
        ["id"],
    )

    op.create_unique_constraint(
        None,
        "users",
        ["username"],
    )


def downgrade():
    op.drop_table("users")
