#!/bin/bash
# Copyright 2021 Florian Burgener
#
# This file is part of Dokumenta.
#
# Dokumenta is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Dokumenta is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Dokumenta.  If not, see <https://www.gnu.org/licenses/>.
"""Create tags table.

Revision ID: 4eb189f85d7e
Revises: 2d00af9617d2
Create Date: 2021-05-18 09:18:02.746479
"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "4eb189f85d7e"
down_revision = "2d00af9617d2"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "tags",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.String(100)),
        sa.Column("category_id", sa.Integer),
        mysql_engine="InnoDB",
    )

    op.create_foreign_key(
        None,
        "tags",
        "categories",
        ["category_id"],
        ["id"],
    )


def downgrade():
    op.drop_table("tags")
