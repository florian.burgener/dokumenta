# Copyright 2021 Florian Burgener
#
# This file is part of Dokumenta.
#
# Dokumenta is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Dokumenta is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Dokumenta.  If not, see <https://www.gnu.org/licenses/>.
"""Definition of different helper functions."""

from configparser import ConfigParser
from typing import List

from mysql.connector.cursor import MySQLCursor

DEFAULT_CONFIG_PATH = "/mnt/d/Ubuntu/etc/dokumenta/dokumenta.ini"
config = None


def load_config(absolute_path: str = DEFAULT_CONFIG_PATH) -> None:
    """Load the application configuration.

    Args:
        absolute_path (str, optional): Absolute path to the
        configuration. Defaults to DEFAULT_CONFIG_PATH.

    Raises:
        ValueError: An error occurred while reading the file.
    """
    global config
    if config is None:
        config = ConfigParser()
        files = config.read(absolute_path)

        if not files:
            raise ValueError("Unable to read the configuration file.")


def get_config() -> ConfigParser:
    """Gets the application's configuration.

    Returns:
        ConfigParser: The configuration of the application.
    """
    global config
    return config


def get_column_names_from_cursor(cursor: MySQLCursor) -> List[str]:
    """Gets the list of columns in the database cursor.

    Args:
        cursor (MySQLCursor): The database cursor.

    Returns:
        List[str]: The list of columns.
    """
    return [column[0] for column in cursor.description]
