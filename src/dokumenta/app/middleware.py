# Copyright 2021 Florian Burgener
#
# This file is part of Dokumenta.
#
# Dokumenta is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Dokumenta is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Dokumenta.  If not, see <https://www.gnu.org/licenses/>.
"""Definitions of middleware.

Definitions of middleware that allow to verify that the requests are
compliant.
"""

from functools import wraps
from typing import Any, Dict, Callable, cast, Optional, Tuple, Type, TypeVar
import os
import re

from flask import jsonify
from flask.wrappers import Response
from flask import request
from jsonschema import validate
import bcrypt
import jwt

from dokumenta.app.helpers import get_config
from dokumenta.app.models import Model
from dokumenta.app.models import User

F = TypeVar("F", bound=Callable[..., Any])


def json_body_middleware(func: F) -> F:
    """Middleware that forces requests to include
    "Content-Type: application/json"."""

    @wraps(func)
    def wrapper(*args: Any, **kwargs: Any) -> Any:
        if (
            "Content-Type" in request.headers
            and request.headers["Content-Type"] == "application/json"
        ):
            return func(*args, **kwargs)
        response_message = (
            "The Content-Type value must be set to application/json."
        )
        return (jsonify({"message": response_message}), 400)

    return cast(F, wrapper)


def resource_404_middleware(model: Type[Model]) -> Callable[[F], F]:
    """Middleware that checks if the model passed in parameter really
    exists in the database."""

    def decorator(func: F) -> F:
        @wraps(func)
        def wrapper(*args: Any, **kwargs: Any) -> Any:
            if model.exists("id", kwargs[model.__name__.lower()]):
                return func(*args, **kwargs)

            response_message = (
                "404 Not Found. This "
                + model.__name__.lower()
                + " does not exist."
            )
            return (jsonify({"message": response_message}), 404)

        return cast(F, wrapper)

    return decorator


def request_validator_middleware(schema: Dict[str, Any]) -> Callable[[F], F]:
    """Middleware that validates data using a JSON Schema."""

    def decorator(func: F) -> F:
        @wraps(func)
        def wrapper(*args: Any, **kwargs: Any) -> Any:
            try:
                validate(instance=request.json, schema=schema)
                return func(*args, **kwargs)
            except:
                return jsonify({"message": "The given data was invalid."}), 422

        return cast(F, wrapper)

    return decorator


def auth_required_middleware(
    auth_type="auto", is_public_route=False
) -> Callable[[F], F]:
    """Middleware that requires the user to authenticate."""

    def decorator(func: F) -> F:
        @wraps(func)
        def wrapper(*args: Any, **kwargs: Any) -> Any:
            config = get_config()
            always_authenticate = config["Security"]["AlwaysAuthenticate"]
            always_authenticate = int(always_authenticate)

            if is_public_route and always_authenticate == 0:
                return func(*args, **kwargs)
            tmp = auth_type

            if tmp == "auto":
                tmp = get_auth_type()
                security_level = int(config["Security"]["SecurityLevel"])

                if security_level == 1:
                    return func(*args, **kwargs)
                levels = {2: "basic", 3: "jwt"}

                if tmp != levels[security_level]:
                    return (
                        jsonify({"message": "Wrong authentication method."}),
                        400,
                    )
            if tmp == "basic":
                response = try_basic_auth()
            if tmp == "jwt":
                response = try_jwt_auth()
            if response is not None:
                return response
            return func(*args, **kwargs)

        return cast(F, wrapper)

    return decorator


def try_basic_auth() -> Optional[Tuple[Response, int]]:
    """Try to connect the user via basic authentication.

    Returns:
        Optional[Tuple[Response, int]]: Nothing or an HTTP response
        containing an error.
    """
    authorization = request.authorization

    if (
        not authorization
        or not authorization.username
        or not authorization.password
    ):
        print(authorization)
        response_message = "The authorization header is not defined."
        return (jsonify({"message": response_message}), 400)
    user = User.find_custom("username", authorization.username)

    if not user:
        return jsonify({"message": "Wrong credentials."}), 401
    request_password = authorization.password.encode("utf-8")

    if not bcrypt.checkpw(request_password, user.password.encode("utf-8")):
        return jsonify({"message": "Wrong credentials."}), 401
    return None


def try_jwt_auth() -> Optional[Tuple[Response, int]]:
    """Try to connect the user via JWT authentication.

    Returns:
        Optional[Tuple[Response, int]]: Nothing or an HTTP response
        containing an error.
    """
    token = get_jwt_token()

    try:
        payload = jwt.decode(token, os.getenv("APP_KEY"), algorithms="HS256")

        if not User.exists("id", payload["user_id"]):
            return jsonify({"message": "Wrong credentials."}), 401
    except:
        return jsonify({"message": "Wrong credentials."}), 401
    return None


def get_auth_type() -> Optional[str]:
    """Gets the type of authentication sent with the request.

    Returns:
        str: The type of authentication.
    """
    expression = r"Bearer .+"
    if "Authorization" in request.headers and re.match(
        expression, request.headers["Authorization"]
    ):
        return "jwt"
    if request.authorization is not None:
        return "basic"
    return None


def get_jwt_token() -> str:
    """Gets the JWT token from the header "Authorization."

    Returns:
        str: The JWT token.
    """
    authorization = request.headers["Authorization"]
    token = authorization.replace("Bearer ", "")
    return token


def role_required_middleware(role_name: str) -> Callable[[F], F]:
    """Middleware that verifies that the authenticated user is of a certain role."""

    def decorator(func: F) -> F:
        @wraps(func)
        def wrapper(*args: Any, **kwargs: Any) -> Any:
            security_level = int(get_config()["Security"]["SecurityLevel"])

            if security_level == 1:
                return func(*args, **kwargs)

            auth_type = get_auth_type()

            if auth_type == "basic":
                user = User.find_custom(
                    "username", request.authorization.username
                )
            if auth_type == "jwt":
                payload = jwt.decode(
                    get_jwt_token(), os.getenv("APP_KEY"), algorithms="HS256"
                )
                user = User.find(payload["user_id"])

            if user.role().name != role_name:
                response_message = (
                    "You are not authorized to perform this action."
                )
                return (jsonify({"message": response_message}), 403)
            return func(*args, **kwargs)

        return cast(F, wrapper)

    return decorator
