# Copyright 2021 Florian Burgener
#
# This file is part of Dokumenta.
#
# Dokumenta is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Dokumenta is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Dokumenta.  If not, see <https://www.gnu.org/licenses/>.
"""List of the controller responsible for processing the requests.

This module defines all the controllers and all the accessible routes.
Controllers are grouped by resource/section in this order: WWW,
Clustering, Category, Document, Role, User
"""

from __future__ import with_statement
from typing import Any, Dict, List, Optional, Tuple, Union
import datetime
import os
import pickle
import time

from flask import g
from flask import jsonify
from flask import render_template
from flask import request
from flask import send_file
from flask.wrappers import Response
import bcrypt
import jwt
import magic

from dokumenta.app.databases import DokumentaDB
from dokumenta.app.middleware import *
from dokumenta.app.models import Category
from dokumenta.app.models import Document
from dokumenta.app.models import DocumentSearcher
from dokumenta.app.models import HierarchicalKMeans
from dokumenta.app.models import Role
from dokumenta.app.models import User
from dokumenta.app.validation import *
from dokumenta.server import app


@app.before_request  # type: ignore
def open_database() -> Optional[Tuple[Response, int]]:
    """Opens a connection to the database otherwise throws an error 500
    is sent.

    Returns:
        Optional[Tuple[Response, int]]: Nothing or an error if the
        connection failed.
    """
    try:
        g.database = DokumentaDB()
    except:
        return make_err_response("500 Internal Server Error", 500)
    return None


@app.teardown_appcontext
def close_database(error: Any) -> None:
    """Closes the connection to the Dokumenta database."""
    if hasattr(g, "database"):
        del g.database


"""
------------------------------------------------------------------------
www
------------------------------------------------------------------------
"""


def get_warning_colors() -> List[str]:
    """List of colors from red to green.

    Returns:
        List[str]: List of colors.
    """
    return [
        "#FF0000",
        "#FF1000",
        "#FF2000",
        "#FF3000",
        "#FF4000",
        "#FF5000",
        "#FF6000",
        "#FF7000",
        "#FF8000",
        "#FF9000",
        "#FFA000",
        "#FFB000",
        "#FFC000",
        "#FFD000",
        "#FFE000",
        "#FFF000",
        "#FFFF00",
        "#F0FF00",
        "#E0FF00",
        "#D0FF00",
        "#C0FF00",
        "#B0FF00",
        "#A0FF00",
        "#90FF00",
        "#80FF00",
        "#70FF00",
        "#60FF00",
        "#50FF00",
        "#40FF00",
        "#30FF00",
        "#20FF00",
        "#10FF00",
    ]


def get_clstrng_warning_color(dp_since_clustering: float) -> str:
    """Gets the color of the current clustering state.

    Args:
        dp_since_clustering (float): Percentage of new documents since
        the last clustering.

    Returns:
        str: The color.
    """
    colors = get_warning_colors()
    index = dp_since_clustering / 1000
    if index > 1:
        index = 1
    index = int((round(len(colors) - 1) - (len(colors) - 1) * index))
    return colors[index]


@app.route("/", methods=["GET"])
def www_index() -> Union[Response, Tuple[Response, int]]:
    """Processes a request to get the home page.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """

    document_count = Document.count()
    category_count = Category.count()
    named_category_count = Category.count("name IS NOT NULL")
    unnamed_category_count = Category.count("name IS NULL")

    date_directory = get_config()["Storage"]["DataDirectory"]
    dcmnt_used_filename = date_directory + "document_used_count.pickle"

    if os.path.isfile(dcmnt_used_filename):
        # Compute the variables that allow to know the state of the
        # clustering.
        with open(dcmnt_used_filename, "rb") as f:
            dc_when_clustering = pickle.load(f)
        dc_since_clustering = document_count - dc_when_clustering
        dp_since_clustering = dc_since_clustering / dc_when_clustering * 100
        dp_since_clustering = round(dp_since_clustering)
        clstrng_warning_color = get_clstrng_warning_color(dp_since_clustering)
    else:
        # Default value on the clustering status because no clustering
        # has ever been performed.
        dc_when_clustering = None
        dc_since_clustering = None
        dp_since_clustering = None
        clstrng_warning_color = None

    return render_template(
        "index.html",
        document_count=document_count,
        category_count=category_count,
        named_category_count=named_category_count,
        unnamed_category_count=unnamed_category_count,
        dc_when_clustering=dc_when_clustering,
        dc_since_clustering=dc_since_clustering,
        dp_since_clustering=dp_since_clustering,
        clstrng_warning_color=clstrng_warning_color,
    )


@app.route("/login", methods=["GET"])
def www_login() -> Union[Response, Tuple[Response, int]]:
    """Processes a request to get the login page.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    return render_template("login.html")


@app.route("/docs/v1", methods=["GET"])
def www_api_documentation() -> Union[Response, Tuple[Response, int]]:
    """Processes a request to get the API documentation page.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    return render_template("docs/v1/index.html")


@app.route("/categories", methods=["GET"])
def www_categories() -> Union[Response, Tuple[Response, int]]:
    """Processes a request to get the category management page.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    return render_template("categories.html")


@app.route("/documents", methods=["GET"])
def www_documents() -> Union[Response, Tuple[Response, int]]:
    """Processes a request to get the document management page.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    return render_template("documents.html")


@app.route("/users", methods=["GET"])
def www_users() -> Union[Response, Tuple[Response, int]]:
    """Processes a request to get the user management page.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    return render_template("users.html")


"""
------------------------------------------------------------------------
Clustering
------------------------------------------------------------------------
"""


@app.route("/api/v1/run-clustering", methods=["POST"])  # type: ignore
@auth_required_middleware("auto")
@role_required_middleware("admin")
def run_clustering() -> Union[Response, Tuple[Response, int]]:
    """Processes a request to perform clustering.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    start_time = time.time()

    g.database.execute("SET FOREIGN_KEY_CHECKS = 0;")
    g.database.execute("TRUNCATE categories")
    g.database.execute("TRUNCATE category_document")
    g.database.execute("TRUNCATE tags")
    g.database.execute("SET FOREIGN_KEY_CHECKS = 1;")

    g.database.execute("SELECT id, clean_text FROM documents")
    documents = g.database.cursor.fetchall()
    print("The documents are loaded")
    if len(documents) < 100:
        response_message = (
            "A minimum of 100 documents is required to perform a clustering."
        )
        return make_err_response(response_message, 403)
    model = HierarchicalKMeans(documents)
    model.fit()

    print("%ss" % (time.time() - start_time))
    return jsonify({"message": "The clustering went well."})


"""
------------------------------------------------------------------------
Resource: Category
------------------------------------------------------------------------
"""


@app.route("/api/v1/categories", methods=["GET"])  # type: ignore
@auth_required_middleware("auto", True)
def category_index() -> Union[Response, Tuple[Response, int]]:
    """Processes a request to read categories.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    with_tags = request.args.get("with_tags")

    if with_tags and with_tags == "1":
        categories = Category.all(
            where="id NOT IN (SELECT category_id FROM categories WHERE category_id IS NOT NULL)"
        )
        categories = Category.all()
        for category in categories:
            category.load_tags()
    else:
        categories = Category.all(where="name IS NOT NULL")
    categories = [x.get_dict() for x in categories]
    return jsonify(categories)


@app.route("/api/v1/categories/<int:category>", methods=["GET"])  # type: ignore
@auth_required_middleware("auto", True)
@resource_404_middleware(Category)
def category_show(category: int) -> Union[Response, Tuple[Response, int]]:
    """Processes a request to read a category.

    Args:
        category (int): The category identifier.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    model = Category.find(category)
    model.load_tags()
    return jsonify({"category": model.get_dict()})


@app.route("/api/v1/categories/<int:category>", methods=["PATCH"])  # type: ignore
@json_body_middleware
@auth_required_middleware("auto")
@role_required_middleware("admin")
@resource_404_middleware(Category)
@request_validator_middleware(category_update_schema())
def category_update(category: int) -> Union[Response, Tuple[Response, int]]:
    """Processes a request to modify a category.

    Args:
        category (int): The category identifier.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    data = request_get(["name"])
    model = Category.update(category, data)
    response_message = "The category has been successfully updated."
    return jsonify({"message": response_message, "category": model.get_dict()})


@app.route("/api/v1/categories/<int:category>", methods=["DELETE"])  # type: ignore
@auth_required_middleware("auto")
@role_required_middleware("admin")
@resource_404_middleware(Category)
def category_destroy(category: int) -> Union[Response, Tuple[Response, int]]:
    """Processes a request to delete a category.

    Args:
        category (int): The category identifier.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    Category.update(category, {"name": None})
    return jsonify({"message": "The category has been successfully deleted."})


"""
------------------------------------------------------------------------
Resource: Document
------------------------------------------------------------------------
"""


@app.route("/api/v1/documents", methods=["POST"])  # type: ignore
@auth_required_middleware("auto")
@role_required_middleware("admin")
def document_create() -> Union[Response, Tuple[Response, int]]:
    """Processes a request to create a document.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    if "title" not in request.form or "file" not in request.files:
        return make_err_response("The given data was invalid.", 422)
    file = request.files["file"]
    file_blob = file.read()

    if len(file_blob) > int(get_config()["Storage"]["MaximumDocumentSize"]):
        return make_err_response("This file is too large.", 413)

    mime = magic.Magic(mime=True)
    mime_type = mime.from_buffer(file_blob)

    if mime_type not in ["application/pdf"]:
        return make_err_response("This file type is not supported.", 415)

    try:
        document = Document.create(
            {
                "title": request.form["title"],
                "file": file,
                "file_type": mime_type,
            }
        )
    except Exception as e:
        response_message = (
            "Your document could not be created because it either already "
            "exists or could not be processed."
        )
        return make_err_response(response_message, 400)

    response_message = "The document has been successfully created."
    return jsonify(
        {"document": document.get_dict(), "message": response_message}
    )


@app.route("/api/v1/documents", methods=["GET"])  # type: ignore
@auth_required_middleware("auto")
@role_required_middleware("admin")
def document_index() -> Union[Response, Tuple[Response, int]]:
    """Processes a request to read documents.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    page = None

    try:
        page = int(request.args["page"])
        if page < 1:
            raise Exception("Impossible page number.")
    except:
        page = 1

    if "sort" in request.args:
        sort = request.args["sort"]
    else:
        sort = "asc"
    documents = Document.paginate(page=page, sort=sort)
    documents = [x.get_dict() for x in documents]
    return jsonify(documents)


@app.route("/api/v1/categories/<int:category>/documents", methods=["GET"])  # type: ignore
@auth_required_middleware("auto", True)
@resource_404_middleware(Category)
def document_index_category(category) -> Union[Response, Tuple[Response, int]]:
    """Processes a request to read documents related to a category.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    category = Category.find(category)
    searcher = DocumentSearcher(category)
    documents = searcher.search(
        page=int(request.args.get("page") or 1),
        page_length=int(request.args.get("page_length") or 10),
    )
    return jsonify([x.get_dict() for x in documents])


@app.route("/api/v1/documents/<int:document>", methods=["GET"])  # type: ignore
@auth_required_middleware("auto", True)
@resource_404_middleware(Document)
def document_show(document: int) -> Union[Response, Tuple[Response, int]]:
    """Processes a request to read a document.

    Args:
        document (int): The document identifier.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    model = Document.find(document)
    return jsonify({"document": model.get_dict()})


@app.route("/api/v1/documents/<int:document>/download", methods=["GET"])  # type: ignore
@auth_required_middleware("auto", True)
@resource_404_middleware(Document)
def document_download(document: int) -> Union[Response, Tuple[Response, int]]:
    """Processes a request to download a document.

    Args:
        document (int): The document identifier.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    model = Document.find(document)

    if not os.path.isfile(model.get_absolute_path()):
        return make_err_response(
            "This document exists but we cannot find it.", 500
        )
    return send_file(model.get_absolute_path())


@app.route("/api/v1/documents/<int:document>", methods=["PATCH"])  # type: ignore
@json_body_middleware
@auth_required_middleware("auto")
@role_required_middleware("admin")
@resource_404_middleware(Document)
@request_validator_middleware(document_update_schema())
def document_update(document: int) -> Union[Response, Tuple[Response, int]]:
    """Processes a request to modify a document.

    Args:
        document (int): The document identifier.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    data = request_get(["title"])
    model = Document.update(document, data)
    response_message = "The document has been successfully updated."
    return jsonify({"message": response_message, "document": model.get_dict()})


@app.route("/api/v1/documents/<int:document>", methods=["DELETE"])  # type: ignore
@auth_required_middleware("auto")
@role_required_middleware("admin")
@resource_404_middleware(Document)
def document_destroy(document: int) -> Union[Response, Tuple[Response, int]]:
    """Processes a request to delete a document.

    Args:
        document (int): The document identifier.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    Document.delete(document)
    response_message = "The document has been successfully deleted."
    return jsonify({"message": response_message})


"""
------------------------------------------------------------------------
Resource: Role
------------------------------------------------------------------------
"""


@app.route("/api/v1/roles", methods=["GET"])  # type: ignore
@auth_required_middleware("auto")
@role_required_middleware("admin")
def role_index() -> Union[Response, Tuple[Response, int]]:
    """Processes a request to read roles.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    roles = [x.get_dict() for x in Role.all()]
    return jsonify(roles)


@app.route("/api/v1/roles/<int:role>", methods=["GET"])  # type: ignore
@auth_required_middleware("auto")
@role_required_middleware("admin")
@resource_404_middleware(Role)
def role_show(role: int) -> Union[Response, Tuple[Response, int]]:
    """Processes a request to read a role.

    Args:
        role (int): The role identifier.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    model = Role.find(role)
    return jsonify({"role": model.get_dict()})


"""
------------------------------------------------------------------------
Resource: User
------------------------------------------------------------------------
"""


@app.route("/api/v1/users", methods=["POST"])  # type: ignore
@json_body_middleware
@auth_required_middleware("auto")
@role_required_middleware("admin")
@request_validator_middleware(user_create_schema())
def user_create() -> Union[Response, Tuple[Response, int]]:
    """Processes a request to create a user.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    if User.exists("username", request.json["username"]):
        return make_err_response("This username is already taken.", 409)
    if not Role.exists("id", request.json["role_id"]):
        return make_err_response("This role does not exist.", 400)
    data = request_get(["username", "password", "role_id"])
    # Hash the password with the bcrypt algorithm (note: the hashing is
    # slow).
    raw_password = data["password"].encode("utf-8")
    data["password"] = bcrypt.hashpw(raw_password, bcrypt.gensalt())
    user = User.create(data)
    response_message = "The user has been successfully created."
    return jsonify({"message": response_message, "user": user.get_dict()})


@app.route("/api/v1/users", methods=["GET"])  # type: ignore
@auth_required_middleware("auto")
@role_required_middleware("admin")
def user_index() -> Union[Response, Tuple[Response, int]]:
    """Processes a request to read users.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    users = [x.get_dict() for x in User.all()]
    return jsonify(users)


@app.route("/api/v1/users/<int:user>", methods=["GET"])  # type: ignore
@auth_required_middleware("auto")
@role_required_middleware("admin")
@resource_404_middleware(User)
def user_show(user: int) -> Union[Response, Tuple[Response, int]]:
    """Processes a request to read a user.

    Args:
        user (int): The user identifier.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    model = User.find(user)
    return jsonify({"user": model.get_dict()})


@app.route("/api/v1/users/<int:user>", methods=["PATCH"])  # type: ignore
@json_body_middleware
@auth_required_middleware("auto")
@role_required_middleware("admin")
@resource_404_middleware(User)
@request_validator_middleware(user_update_schema())
def user_update(user: int) -> Union[Response, Tuple[Response, int]]:
    """Processes a request to modify a user.

    Args:
        user (int): The user identifier.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    if User.exists("username", request.json["username"]):
        return make_err_response("This username is already taken.", 409)
    if not Role.exists("id", request.json["role_id"]):
        return make_err_response("This role does not exist.", 400)
    data = request_get(["username", "password", "role_id"])

    if "password" in data:
        # If the password is in the data to be updated then the new
        # password is hashed.
        raw_password = data["password"].encode("utf-8")
        data["password"] = bcrypt.hashpw(raw_password, bcrypt.gensalt())
    model = User.update(user, data)
    response_message = "The user has been successfully updated."
    return jsonify({"message": response_message, "user": model.get_dict()})


@app.route("/api/v1/users/<int:user>", methods=["DELETE"])  # type: ignore
@auth_required_middleware("auto")
@role_required_middleware("admin")
@resource_404_middleware(User)
def user_destroy(user: int) -> Union[Response, Tuple[Response, int]]:
    """Processes a request to delete a user.

    Args:
        user (int): The user identifier.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    User.delete(user)
    response_message = "The user has been successfully deleted."
    return jsonify({"message": response_message})


"""
------------------------------------------------------------------------
Controllers for authentication
------------------------------------------------------------------------
"""


@app.route("/api/v1/auth/login", methods=["POST"])  # type: ignore
@auth_required_middleware("basic")
def auth_login():
    """Processes a login request to obtain a JWT token.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    user = User.find_custom("username", request.authorization.username)
    payload = {
        "user_id": user.id,
        "exp": datetime.datetime.utcnow() + datetime.timedelta(minutes=120),
    }
    token = jwt.encode(payload, os.getenv("APP_KEY"), algorithm="HS256")
    return {
        "message": "The authentication was successful.",
        "token": token,
        "user": user.get_dict(),
    }


"""
------------------------------------------------------------------------
Controllers for testing
------------------------------------------------------------------------
"""


@app.route("/api/v1/testing/basic/login", methods=["POST"])  # type: ignore
@auth_required_middleware("basic")
@role_required_middleware("admin")
def testing_basic_login() -> Union[Response, Tuple[Response, int]]:
    """Processes a basic login request for testing purposes.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    response_message = "The user has been successfully authenticated."
    return jsonify({"message": response_message})


@app.route("/api/v1/testing/jwt/login", methods=["POST"])  # type: ignore
@auth_required_middleware("jwt")
def testing_jtw_login() -> Union[Response, Tuple[Response, int]]:
    """Processes a JWT login request for testing purposes.

    Returns:
        Union[Response, Tuple[Response, int]]: A response to the
        request.
    """
    response_message = "The user has been successfully authenticated."
    return jsonify({"message": response_message})


"""
------------------------------------------------------------------------
Helper Functions
------------------------------------------------------------------------
"""


def request_get(attributes: List[str]) -> Dict[str, Any]:
    """Retrieves the specified attributes from the request.

    Args:
        attributes (List[str]): The attributes to be retrieved from the
        request.

    Returns:
        Dict[str, Any]: The attributes that have been retrieved from
        the request.
    """
    data = {}

    for attribute in attributes:
        if attribute in request.json:
            data[attribute] = request.json[attribute]
    return data


def make_err_response(
    message: str, response_code: int
) -> Tuple[Response, int]:
    """Generates an error response.

    Args:
        message (str): The error message.
        response_code (int): The HTTP status code.

    Returns:
        Tuple[Response, int]: An error response.
    """
    return jsonify({"message": message}), response_code
