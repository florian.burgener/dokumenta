# Copyright 2021 Florian Burgener
#
# This file is part of Dokumenta.
#
# Dokumenta is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Dokumenta is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Dokumenta.  If not, see <https://www.gnu.org/licenses/>.
"""Definitions of JSON schemas.

Definition of JSON schemas for requests that need validation of their
data.
"""

from typing import Any, Dict


def category_update_schema() -> Dict[str, Any]:
    """JSON validation schema for the modification of a category.

    Returns:
        Dict[str, Any]: The JSON validation schema.
    """
    return {
        "type": "object",
        "properties": {
            "name": {
                "type": "string",
            }
        },
    }


def user_create_schema() -> Dict[str, Any]:
    """JSON validation schema for the creation of an user.

    Returns:
        Dict[str, Any]: The JSON validation schema.
    """
    return {
        "type": "object",
        "properties": {
            "username": {
                "type": "string",
            },
            "password": {
                "type": "string",
            },
            "role_id": {
                "type": "integer",
            },
        },
        "required": ["username", "password", "role_id"],
    }


def user_update_schema() -> Dict[str, Any]:
    """JSON validation schema for the modification of an user.

    Returns:
        Dict[str, Any]: The JSON validation schema.
    """
    return {
        "type": "object",
        "properties": {
            "username": {
                "type": "string",
            },
            "password": {
                "type": "string",
            },
            "role_id": {
                "type": "integer",
            },
        },
    }


def document_update_schema() -> Dict[str, Any]:
    """JSON validation schema for the modification of a document.

    Returns:
        Dict[str, Any]: The JSON validation schema.
    """
    return {
        "type": "object",
        "properties": {
            "title": {
                "type": "string",
            }
        },
        "required": ["title"],
    }
