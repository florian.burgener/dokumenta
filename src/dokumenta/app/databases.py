# Copyright 2021 Florian Burgener
#
# This file is part of Dokumenta.
#
# Dokumenta is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Dokumenta is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Dokumenta.  If not, see <https://www.gnu.org/licenses/>.
"""Definition of the class that interacts with the database.

This module defines the class that allows to interact with the Dokumenta
database.
"""

from typing import Any, Tuple
import os

import mysql.connector


class DokumentaDB:
    """Represents a connection to the Dokumenta database."""

    def __init__(self) -> None:
        """Inits DokumentaDB."""
        self.cnx = mysql.connector.connect(
            host=os.getenv("DB_HOST"),
            database=os.getenv("DB_DATABASE"),
            user=os.getenv("DB_USERNAME"),
            passwd=os.getenv("DB_PASSWORD"),
        )
        self.cursor = self.cnx.cursor()

        # Initializes various session variables.
        self.execute("SET AUTOCOMMIT = 1")
        self.execute("SET NAMES utf8mb4")
        self.execute("SET CHARACTER SET utf8mb4")
        self.execute("SET character_set_connection=utf8mb4")
        self.execute('SET time_zone="+00:00"')

    def execute(self, query: str, params: Tuple[Any, ...] = ()) -> None:
        """Executes a query on the database.

        Args:
            query (str): The request to be executed.
            params (Tuple[Any, ...]): The parameters to send with the request.
            Defaults to ().
        """
        self.cursor.execute(query, params)

    def __del__(self) -> None:
        """Closes the connection when the object is destroyed."""
        self.cnx.close()
