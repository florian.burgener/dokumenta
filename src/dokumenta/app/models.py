# Copyright 2021 Florian Burgener
#
# This file is part of Dokumenta.
#
# Dokumenta is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Dokumenta is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Dokumenta.  If not, see <https://www.gnu.org/licenses/>.
"""All the models and therefore almost all the logic of the application
is defined here.

This module contains models representing the database tables as well
as other useful classes for the different features of Dokumenta.
"""

from __future__ import annotations

from abc import ABC
from typing import Any, Dict, List, Optional, Type, TypeVar
import abc
import hashlib
import math
import os
import pickle

from flask import g
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from sklearn.cluster import MiniBatchKMeans
from sklearn.feature_extraction.text import TfidfVectorizer
import pdftotext

from dokumenta.app.helpers import get_column_names_from_cursor
from dokumenta.app.helpers import get_config

T = TypeVar("T", bound="Model")


class Model:
    """Represents any table in the database."""

    id: int

    def __init__(self, columns: List[str], row: List[Any]) -> None:
        """Inits Model.

        Args:
            columns (List[str]): Column of the table.
            row (List[Any]): Data of one line.
        """
        self.hidden_columns = ["hidden_columns"]

        # Uses the name of the columns to initialize the member
        # variables of the class.
        for i in range(len(columns)):
            setattr(self, columns[i], row[i])

    def get_dict(self) -> Dict[str, Any]:
        """Converts the data of the class into a dictionary.

        Returns:
            Dict[str, Any]: A dictionary containing the data of the
            class.
        """
        tmp = self.__dict__.copy()

        for column in self.hidden_columns:
            tmp.pop(column, None)
        return tmp

    @classmethod
    def get_table_name(cls: Type[T]) -> str:
        """Get the table name from the class name.

        Returns:
            string: The name of the table corresponding to the class.
        """
        table_name = cls.__name__.lower()
        irregular_plurar_nouns = {"category": "categories"}

        if table_name in irregular_plurar_nouns:
            # In some cases it is not possible to use an algorithm to
            # convert the class name to plural, for example "Category" is irregular.
            return irregular_plurar_nouns[table_name]
        return table_name + "s"

    @classmethod
    def create_instance(
        cls: Type[T], columns: List[str], data: List[Any]
    ) -> T:
        """Create an instance from the provided data.

        Args:
            columns (List[str]): The columns of the table to be
            reproduced in the model.
            data (List[Any]): The data of an entry in the table.

        Returns:
            T: An instance of a model.
        """
        if data is None:
            # Don't do anything if there is no data.
            return None
        model = cls(columns, data)
        return model

    @classmethod
    def instantiate_data(
        cls: Type[T], columns: List[str], data: List[Any]
    ) -> List[T]:
        """Instantiates several models at once from the list of data provided.

        Args:
            columns (List[str]): The columns of the table to be
            reproduced in the model.
            data (List[Any]): The list of data to instantiate.

        Returns:
            List[T]: List of instanciated models.
        """
        models = []

        for item in data:
            model = cls.create_instance(columns, item)
            models.append(model)
        return models

    @classmethod
    def create(cls: Type[T], data: Dict[str, Any]) -> T:
        """Create a new entry in the database.

        Args:
            data (Dict[str, Any]): The data needed to create the new
            entry.

        Returns:
            Type[T]: The instance of the created model.
        """
        columns = []
        row = []

        for key, value in data.items():
            columns.append(key)
            row.append(value)
        query = (
            "INSERT INTO "
            + cls.get_table_name()
            + " ("
            + ", ".join(columns)
            + ") VALUES ("
            + ("%s, " * len(columns))[:-2]
            + ")"
        )
        g.database.execute(query, tuple(row))

        return cls.find(g.database.cursor.lastrowid)

    @classmethod
    def exists(
        cls: Type[T], key: str, value: Any, where: Optional[str] = None
    ) -> bool:
        """Checks if an entry exists in the database.

        Args:
            key (str): The name of the column to check
            value (Any): The value of the column to check.
            where (Optional[str], optional): Additional conditions.
            Defaults to None.

        Returns:
            bool: True = the entry exists, False = the entry does not exist.
        """
        g.database.execute(
            "SELECT EXISTS(SELECT * FROM "
            + cls.get_table_name()
            + " WHERE "
            + key
            + " = %s"
            + (" " + where if where is not None else "")
            + ")",
            (value,),
        )
        exists = g.database.cursor.fetchone()[0]
        return bool(exists)

    @classmethod
    def first(cls: Type[T]) -> T:
        """Retrieves the first entry in the table.

        Returns:
            T: The instance of the first model found.
        """
        query = "SELECT * FROM " + cls.get_table_name() + " LIMIT 1"
        g.database.execute(query)
        columns = get_column_names_from_cursor(g.database.cursor)
        data = g.database.cursor.fetchone()
        return cls.create_instance(columns, data)

    @classmethod
    def find(cls: Type[T], id: int) -> T:
        """Find the model corresponding to the ID provided.

        Args:
            id (int): The identifier of the model to find.

        Returns:
            cls: The instance of the found model.
        """
        return cls.find_custom("id", id)

    @classmethod
    def find_custom(cls: Type[T], key: str, value: Any, where: str = "") -> T:
        """Find the model corresponding to data provided.

        Args:
            key (str): The name of the column in which the search is performed.
            value (Any): The value that must be searched for.
            where (str, optional): Additional conditions.
            Defaults to "".

        Returns:
            T: The instance of the found model.
        """
        query = (
            "SELECT * FROM "
            + cls.get_table_name()
            + " WHERE "
            + key
            + " = %s "
            + where
            + " LIMIT 1"
        )
        g.database.execute(query, (value,))
        columns = get_column_names_from_cursor(g.database.cursor)
        data = g.database.cursor.fetchone()
        return cls.create_instance(columns, data)

    @classmethod
    def all(
        cls: Type[T], where: Optional[str] = None, limit: Optional[str] = None
    ) -> List[T]:
        """Retrieves all the data in the table.

        Args:
            where (Optional[str], optional): Additional conditions.
            Defaults to None.
            limit (Optional[str], optional): Optional limit. Defaults to
            None.

        Returns:
            List[T]: The list of all models corresponding to the data in
            the table.
        """
        query = "SELECT * FROM " + cls.get_table_name()

        if where is not None:
            query += " WHERE " + where
        if limit is not None:
            query += " LIMIT " + limit
        g.database.execute(query)
        columns = get_column_names_from_cursor(g.database.cursor)
        data = g.database.cursor.fetchall()
        return cls.instantiate_data(columns, data)

    @classmethod
    def count(cls: Type[T], where: Optional[str] = None) -> int:
        """Counts the number of entries of the resource.

        Args:
            where (Optional[str], optional): Additional conditions.
            Defaults to None.

        Returns:
            int: The number of entries of the resource.
        """

        query = "SELECT COUNT(*) FROM " + cls.get_table_name()

        if where is not None:
            query += " WHERE " + where
        g.database.execute(query)
        count = g.database.cursor.fetchone()[0]
        return count

    @classmethod
    def update(cls: Type[T], id: int, data: Dict[str, Any]) -> T:
        """Modifies an entry in the database.

        Args:
            id (int): The identifier of the entry to be modified.
            data (Dict[str, Any]): The data to be modified.

        Returns:
            T: The instance of the model that has been modified.
        """
        columns = []
        row = []

        for key, value in data.items():
            columns.append(key)
            row.append(value)
        query = (
            "UPDATE "
            + cls.get_table_name()
            + " SET "
            + " = %s, ".join(columns)
            + " = %s WHERE id = %s"
        )
        g.database.execute(query, tuple(row) + (id,))
        return cls.find(id)

    @classmethod
    def delete(cls: Type[T], id: int) -> None:
        """Deletes an entry from the database.

        Args:
            id (int): The identifier corresponding to the data to be
            deleted.
        """
        g.database.execute(
            "DELETE FROM " + cls.get_table_name() + " WHERE id = %s", (id,)
        )


class Category(Model):
    """Represents a model for the "categories" table."""

    name: str

    def __init__(self, columns: List[str], row: List[Any]):
        """Inits Category.

        Args:
            columns (List[str]): Column of the table.
            row (List[Any]): Data of one line.
        """
        super().__init__(columns, row)

    def load_tags(self):
        """Load tags."""
        self.tags = Tag.all(where="category_id = " + str(self.id))

    def count_documents(self) -> int:
        """Count the associated documents.

        Returns:
            int: The count.
        """

        query = "SELECT COUNT(*) FROM category_document WHERE category_id = %s"
        g.database.execute(query, (self.id,))
        return g.database.cursor.fetchone()[0]

    def get_dict(self) -> Dict[str, Any]:
        """Converts the data of the class into a dictionary.

        Returns:
            Dict[str, Any]: A dictionary containing the data of the
            class.
        """
        tmp = super().get_dict()
        tmp["document_count"] = self.count_documents()

        if hasattr(self, "tags"):
            tmp["tags"] = [x.get_dict() for x in self.tags]
        return tmp

    @classmethod
    def delete(cls, id: int) -> None:
        """Deletes an entry from the database.

        Args:
            id (int): The identifier corresponding to the data to be
            deleted.
        """
        category = cls.find(id)

        if category.category_id is None:
            for subcategory in category.subcategories:
                super().delete(subcategory.id)
        super().delete(id)


class Document(Model):
    """Represents a model for the "documents" table."""

    title: str
    relative_path: str
    filename: str
    file_type: str
    raw_text: str
    clean_text: str

    def __init__(self, columns: List[str], row: List[Any]) -> None:
        """Inits Document.

        Args:
            columns (List[str]): Column of the table.
            row (List[Any]): Data of one line.
        """
        super().__init__(columns, row)
        self.hidden_columns += [
            "relative_path",
            "filename",
            "raw_text",
            "clean_text",
        ]

    def get_dict(self) -> Dict[str, Any]:
        """Converts the data of the class into a dictionary.

        Returns:
            Dict[str, Any]: A dictionary containing the data of the
            class.
        """
        tmp = super().get_dict()
        host = get_config()["Network"]["Host"]
        port = get_config()["Network"]["Port"]
        base_url = "http://" + host + ":" + port
        tmp["file_location"] = (
            base_url + "/api/v1/documents/" + str(self.id) + "/download"
        )
        tmp["categories"] = [x.get_dict() for x in self.get_categories()]
        return tmp

    def get_absolute_path(self) -> str:
        """Gets the absolute path to the file.

        Returns:
            str: The absolute path to the file.
        """
        return get_config()["Storage"]["DataDirectory"] + self.relative_path

    def categorize(self, parent_category_id: int = None) -> None:
        """Categorizes the document.

        Args:
            parent_category_id (int, optional): Leave to None. Defaults
            to None.
        """
        data_directory = get_config()["Storage"]["DataDirectory"]
        filename = "clustering.pickle"

        if parent_category_id is not None:
            filename = str(parent_category_id) + "." + filename

        if not os.path.isfile(data_directory + filename):
            return

        with open(data_directory + filename, "rb") as f:
            model, vectorizer, cluster_category_dict = pickle.load(f)
        X = vectorizer.transform([self.clean_text])[0]
        category_id = cluster_category_dict[model.predict(X)[0]]
        query = "INSERT INTO category_document (category_id, document_id) VALUES (%s, %s)"
        data = (category_id, self.id)
        g.database.execute(query, data)

        query = (
            "SELECT EXISTS(SELECT * FROM categories WHERE category_id = %s)"
        )
        g.database.execute(query, (category_id,))
        exists = bool(g.database.cursor.fetchone()[0])

        if exists:
            self.categorize(category_id)

    def get_categories(self) -> List[Category]:
        """Gets the list of categories.

        Returns:
            List[Category]: The list of categories.
        """

        query = (
            "SELECT category_id FROM category_document WHERE document_id = %s"
        )
        g.database.execute(query, (self.id,))
        category_identifiers = g.database.cursor.fetchall()
        categories = []
        for category_identifier in category_identifiers:
            categories.append(Category.find(category_identifier[0]))
        return categories

    @classmethod
    def create(cls, data: Dict[str, Any]) -> Document:
        """Create a new entry in the database.

        Args:
            data (Dict[str, Any]): The data needed to create the new
            entry.

        Returns:
            Type[T]: The instance of the created model.
        """
        filename, relative_path, absolute_path = cls.save_file(data)
        query = "SELECT EXISTS(SELECT * FROM documents WHERE filename = %s)"
        g.database.execute(query, (filename,))
        exists = g.database.cursor.fetchone()[0]

        if exists:
            raise Exception("The document already exists.")

        extractor = TextExtractorContext(PortableDocumentFormatStrategy())

        try:
            raw_text = extractor.extract_text(absolute_path)
            clean_text = Document.clean_raw_text(raw_text)
        except:
            raise Exception("The text of the document could not be extracted.")

        inserted_data = {
            "title": data["title"],
            "relative_path": relative_path,
            "filename": filename,
            "file_type": data["file_type"],
            "raw_text": raw_text,
            "clean_text": clean_text,
        }
        document = super().create(inserted_data)
        document.categorize()
        return document

    @classmethod
    def save_file(cls, data: Dict[str, Any]) -> None:
        """Save the document file in the data directory.

        Args:
            data (Dict[str, Any]): The data in the file.
        """
        file_types = {"application/pdf": "pdf"}
        data["file"].seek(0)
        h = hashlib.sha256()
        h.update(data["file"].read())
        file_hash = h.hexdigest()
        filename = file_hash + "." + file_types[data["file_type"]]
        relative_path = "documents/" + filename
        data_directory = get_config()["Storage"]["DataDirectory"]
        absolute_path = data_directory + relative_path
        data["file"].seek(0)
        data["file"].save(absolute_path)

        return filename, relative_path, absolute_path

    @staticmethod
    def clean_raw_text(raw_text: str) -> str:
        """Cleans the raw text.

        Args:
            raw_text (str): The raw text that need to be cleaned.

        Returns:
            str: Cleaned text.
        """
        stop_words = set(stopwords.words("english"))
        lemmatizer = WordNetLemmatizer()

        document = raw_text.encode("ascii", "ignore").decode()
        tokens = word_tokenize(document)
        words = []

        for word in tokens:
            w = word.lower()
            if not w.isalpha() or w in stop_words or len(w) < 3:
                continue
            w = lemmatizer.lemmatize(w)
            words.append(w)
        return " ".join(words)

    @classmethod
    def all(
        cls, where: Optional[str] = None, limit: Optional[str] = "10"
    ) -> List[Document]:
        """Retrieves all the data in the table.

        Args:
            where (Optional[str], optional): Additional conditions.
            Defaults to None.
            limit (Optional[str], optional): Optional limit. Defaults to
            None.

        Returns:
            List[T]: The list of all models corresponding to the data in
            the table.
        """
        if where is not None:
            return super().all(where=where, limit=limit)
        return super().all(limit=limit)

    @classmethod
    def paginate(
        cls, page: int = 1, page_length: int = 10, sort="asc"
    ) -> List[Document]:
        """Paginate the results of documents.

        Args:
            page (int, optional): The number of the desired page.
            Defaults to 1.
            page_length (int, optional): The number of items per page.
            Defaults to 10.

        Returns:
            List[Document]: The list of paginated documents.
        """
        if sort == "desc":
            orderby = "1 ORDER BY id DESC "
        elif sort == "asc":
            orderby = "1 ORDER BY id ASC "
        else:
            orderby = "1 ORDER BY id ASC "

        return cls.all(
            where=orderby,
            limit=str((page - 1) * page_length) + ", " + str(page_length),
        )

    @classmethod
    def delete(cls: Type[T], id: int) -> None:
        """Deletes an entry from the database.

        Args:
            id (int): The identifier corresponding to the data to be
            deleted.
        """
        document = Document.find(id)

        if os.path.isfile(document.get_absolute_path()):
            os.remove(document.get_absolute_path())
        g.database.execute(
            "DELETE FROM category_document WHERE document_id = %s", (id,)
        )
        super().delete(id)


class Role(Model):
    """Represents a model for the "roles" table."""

    name: str


class Tag(Model):
    """Represents a model for the "tags" table."""

    name: str

    def __init__(self, columns: List[str], row: List[Any]):
        """Inits Tag.

        Args:
            columns (List[str]): Column of the table.
            row (List[Any]): Data of one line.
        """
        super().__init__(columns, row)
        self.hidden_columns += ["id", "category_id"]


class User(Model):
    """Represents a model for the "users" table."""

    username: str
    password: str
    role_id: int

    def __init__(self, columns: List[str], row: List[Any]):
        """Inits User.

        Args:
            columns (List[str]): Column of the table.
            row (List[Any]): Data of one line.
        """
        super().__init__(columns, row)
        self.hidden_columns += ["password", "role_id"]

    def get_dict(self) -> Dict[str, Any]:
        """Converts the data of the class into a dictionary.

        Returns:
            Dict[str, Any]: A dictionary containing the data of the
            class.
        """
        tmp = super().get_dict()
        tmp["role"] = self.role().get_dict()
        return tmp

    def role(self) -> Role:
        """Get role relation.

        Returns:
            Role: The role corresponding to the user.
        """
        return Role.find(self.role_id)


"""
------------------------------------------------------------------------
Document Searcher
------------------------------------------------------------------------
"""


class DocumentSearcher:
    """Represents a class that performs searches based on a category in
    the document table."""

    def __init__(self, category: Category) -> None:
        """Inits DocumentsSearcher.

        Args:
            category (Category): The category with which the search
            should be done.
        """
        self.category = category

    def search(self, page: int = 1, page_length: int = 10) -> List[Document]:
        """Search for documents that meet the criteria of the query.

        Args:
            page (int): The number of the desired page.
            page_length (int): The number of documents per page.

        Returns:
            List[Document]: The documents found.
        """

        offset = (page - 1) * page_length
        limit = str(offset) + ", " + str(page_length)
        query = """
        SELECT documents.* FROM documents
        INNER JOIN category_document ON category_document.document_id = documents.id
        WHERE category_document.category_id = %s
        LIMIT """
        query += limit

        g.database.execute(query, (self.category.id,))
        columns = get_column_names_from_cursor(g.database.cursor)
        data = g.database.cursor.fetchall()
        return Document.instantiate_data(columns, data)


"""
------------------------------------------------------------------------
Text Extraction
------------------------------------------------------------------------
"""


class ITextExtractorStrategy(ABC):
    """Common interface for all text extraction strategies."""

    @abc.abstractmethod
    def extract_text(self, absolute_path: str) -> Optional[str]:
        """Skeleton of the text extraction method.

        Args:
            absolute_path (str): The absolute path to the target file.

        Returns:
            Optional[str]: The text extracted from the file.
        """
        pass


class PortableDocumentFormatStrategy(ITextExtractorStrategy):
    """Implementation of text extraction for PDF files."""

    def extract_text(self, absolute_path: str) -> Optional[str]:
        """Implementation of text extraction for PDF files.

        Args:
            absolute_path (str): The absolute path to the target file.

        Returns:
            Optional[str]: The text extracted from the file.
        """
        text = ""

        try:
            with open(absolute_path, "rb") as f:
                pdf = pdftotext.PDF(f)
                text = "\n".join(pdf)
        except:
            return None
        return text


class TextExtractorContext:
    """Represents a class implementing the strategy design pattern that
    extracts the text of a file."""

    def __init__(self, strategy: ITextExtractorStrategy) -> None:
        """Inits TextExtractorContext.

        Args:
            strategy (ITextExtractorStrategy): The text
            extraction strategy to be used for extraction.
        """
        self._strategy = strategy

    def extract_text(self, absolute_path: str) -> Optional[str]:
        """Executes the strategy.

        Args:
            absolute_path (str): The absolute path to the target file.

        Returns:
            Optional[str]: The text extracted from the file.
        """
        return self._strategy.extract_text(absolute_path)


"""
------------------------------------------------------------------------
Clustering
------------------------------------------------------------------------
"""


class HierarchicalKMeans:
    """Represents a class that implements the Dokumenta categorization algorithm.."""

    def __init__(
        self, documents, depth=1, category_id=None, document_count=None
    ) -> None:
        """Inits HierarchicalKMeans.

        Args:
            documents ([type]): List of documents.
            depth (int, optional): Current recursion depth.
            Defaults to 1.
            category_id ([type], optional): Parent category id.
            Defaults to None.
            document_count ([type], optional): Document count.
            Defaults to None.
        """
        self.documents = documents
        self.depth = depth
        self.category_id = category_id
        if document_count is None:
            self.document_count = len(self.documents)
        else:
            self.document_count = document_count
        self.vectorizer = None
        self.model = None

    def fit(self) -> None:
        """Performs clustering."""
        self.vectorizer = TfidfVectorizer(
            max_features=10000,
            max_df=0.9,
            min_df=5,
            stop_words=["fig", "figure"],
        )
        X = self.vectorizer.fit_transform(
            document[1] for document in self.documents
        )
        X = X.todense()
        self.model = self.find_best_model(X)
        if self.model is None:
            return
        print(self.model.n_clusters)

        cluster_category_dict = self.create_categories()
        categories_dict = self.categorize_documents(cluster_category_dict, X)
        self.save_model(cluster_category_dict)

        if self.depth >= 10:
            return
        for category_id, identifiers in categories_dict.items():
            if len(identifiers) / self.document_count <= 0.25:
                continue
            documents = self.get_documents_from_identifiers(identifiers)
            model = HierarchicalKMeans(
                documents, self.depth + 1, category_id, self.document_count
            )
            model.fit()

    def find_best_model(self, X: List[List[float]]) -> MiniBatchKMeans:
        """Find the best model.

        Args:
            X (List[List[float]]): Vectorized documents.

        Returns:
            MiniBatchKMeans: The best model.
        """
        max_clusters = 25
        Ks = range(2, max_clusters + 1)
        current_model = None
        previous_model = None

        for K in Ks:
            BS = 1000
            current_model = MiniBatchKMeans(
                n_clusters=K, random_state=1, batch_size=BS
            )
            for i in range(int(math.ceil(len(X) / BS))):
                current_model.partial_fit(X[i * BS : (i + 1) * BS, :])
            if K == max_clusters:
                break
            if self.is_overlapping(current_model):
                current_model = previous_model
                break
            previous_model = current_model
        return current_model

    def extract_clusters(
        self, model: MiniBatchKMeans, n_features: int = 10
    ) -> List[List[str]]:
        """Extract the clusters.

        Args:
            model (MiniBatchKMeans): The best model.
            n_features (int, optional): Number of features.
            Defaults to 10.

        Returns:
            List[List[str]]: List of clusters.
        """
        order_centroids = model.cluster_centers_.argsort()[:, ::-1]
        features = self.vectorizer.get_feature_names()
        clusters = []
        for i in range(model.n_clusters):
            cluster = []
            for j in order_centroids[i, :n_features]:
                cluster.append(features[j])
            clusters.append(cluster)
        return clusters

    def is_overlapping(self, model: MiniBatchKMeans) -> bool:
        """Checks if two clusters overlap.

        Args:
            model (MiniBatchKMeans): The best model.

        Returns:
            bool: True = overlap, False = no overlap.
        """
        clusters = self.extract_clusters(model, 3)
        for i in range(model.n_clusters - 1):
            for j in range(i + 1, model.n_clusters):
                a = clusters[i]
                b = clusters[j]
                shared_items = [x for x in b if x in a]
                if len(shared_items) >= 2:
                    return True
        return False

    def create_categories(self) -> Dict[int, int]:
        """Create categories.

        Returns:
            Dict[int, int]: Dictionary cluster index/category.
        """
        clusters = self.extract_clusters(self.model)
        cluster_category_dict = {}
        for i, cluster in enumerate(clusters):
            category = Category.create(
                {}
                if self.category_id is None
                else {"category_id": self.category_id}
            )
            cluster_category_dict[i] = category.id

            for feature in cluster:
                Tag.create({"name": feature, "category_id": category.id})
        return cluster_category_dict

    def categorize_documents(
        self, cluster_category_dict: Dict[int, int], X: List[List[float]]
    ) -> Dict[int, List[int]]:
        """Categorize the documents.

        Args:
            cluster_category_dict (Dict[int, int]): Dictionary cluster
            index/category.
            X (List[List[float]]): Vectorized documents.

        Returns:
            Dict[int, List[int]]: Category/document dictionary.
        """
        X = self.model.predict(X)
        categories_dict = {}
        for i, document in enumerate(self.documents):
            category_id = cluster_category_dict[X[i]]
            document_id = document[0]

            if not category_id in categories_dict:
                categories_dict[category_id] = []
            categories_dict[category_id].append(document_id)

            query = "INSERT INTO category_document (category_id, document_id) VALUES (%s, %s)"
            data = (category_id, document_id)
            g.database.execute(query, data)
        return categories_dict

    def get_documents_from_identifiers(
        self, identifiers: List[int]
    ) -> List[Any]:
        """Retrieves the documents corresponding to the identifiers.

        Args:
            identifiers (List[int]): List of document identifiers.

        Returns:
            List[Any]: List of documents.
        """
        return [x for x in self.documents if x[0] in identifiers]

    def save_model(self, cluster_category_dict):
        """Saves the model data.

        Args:
            cluster_category_dict (Dict[int, int]): Dictionary cluster
            index/category.
        """
        data_directory = get_config()["Storage"]["DataDirectory"]
        filename = "clustering.pickle"

        if self.category_id is not None:
            filename = str(self.category_id) + "." + filename
        with open(data_directory + filename, "wb") as f:
            pickle.dump(
                (self.model, self.vectorizer, cluster_category_dict), f
            )

        if self.category_id is None:
            with open(
                data_directory + "document_used_count.pickle", "wb"
            ) as f:
                pickle.dump(self.document_count, f)
