axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token');
axios.interceptors.response.use(null, function (error) {
    if (error.response.status == 401) {
        logout();
    }

    return Promise.reject(error);
});

$('#usernameTopRight').text(JSON.parse(localStorage.getItem('user')).username);
$('#logoutButton').click(function () {
    logout();
});

function logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    window.location.replace("/login");
}
