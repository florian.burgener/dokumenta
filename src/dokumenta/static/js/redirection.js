var token = localStorage.getItem('token');

if (token === null) {
    window.location.replace("/login");
}
