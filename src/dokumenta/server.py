# Copyright 2021 Florian Burgener
#
# This file is part of Dokumenta.
#
# Dokumenta is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Dokumenta is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Dokumenta.  If not, see <https://www.gnu.org/licenses/>.
"""Module that start the Flask server."""

import argparse

from dotenv import load_dotenv
from flask import Flask
from flask_cors import CORS
import nltk

from dokumenta.app.helpers import get_config
from dokumenta.app.helpers import load_config


app = Flask(__name__)
CORS(app)

# The "controllers.py" module needs to know the "app" variable to define
# the routes of the application, that's why it is imported after.
from dokumenta.app.controllers import *


def main(args: argparse.Namespace) -> None:
    """Entry point of the application.

    Args:
        args (argparse.Namespace): Arguments parsed.
    """
    load_dotenv()
    nltk.download("stopwords")
    if args.config is not None:
        load_config(args.config)
    else:
        load_config()
    config = get_config()
    app.run(
        host=config["Network"]["Host"],
        port=config["Network"]["Port"],
        debug=False,
    )
